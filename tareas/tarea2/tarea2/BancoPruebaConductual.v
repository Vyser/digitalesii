`timescale 	1s	/ 100ps
// escala	unidad temporal (valor de "#1") / precisi�n

// includes de archivos de verilog
// Pueden omitirse y llamarse desde el makefile
`include "mux.v"
`include "probador.v"

module BancoPruebas; // Testbench
	// Por lo general, las se�ales en el banco de pruebas son wires.
	// No almacenan un valor, son manejadas por otras instancias de m�dulos.
	wire reset_L, selector;
	wire[1:0] data_in0, data_in1;
	wire[1:0] data_out;

	// Descripci�n conductual de alarma
	mux	muxPrueba(/*AUTOINST*/
			  // Outputs
			  .data_out		(data_out[1:0]),
			  // Inputs
			  .clk			(clk),
			  .reset_L		(reset_L),
			  .selector		(selector),
			  .data_in0		(data_in0[1:0]),
			  .data_in1		(data_in1[1:0]));
	// Probador: generador de se�ales y monitor
	probador prob (/*AUTOINST*/
		       // Outputs
		       .clk		(clk),
		       .reset_L		(reset_L),
		       .selector	(selector),
		       .data_in0	(data_in0[1:0]),
		       .data_in1	(data_in1[1:0]),
		       // Inputs
		       .data_out	(data_out[1:0]));
endmodule
