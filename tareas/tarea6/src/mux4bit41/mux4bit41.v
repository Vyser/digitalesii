`include "src/mux4bit21/mux4bit21.v"

module mux4bit41( input clk,
            input reset_L,
            input [1:0] selector,
            input [3:0] data_in0,
            input valid_0,
            input [3:0] data_in1,
            input valid_1,
            input [3:0] data_in2,
            input valid_2,
            input [3:0] data_in3,
            input valid_3,
            output valid_out,
            output [3:0] data_out_cond
      );
      wire [3:0] outMux0, outMux1;
      wire validOut0, validOut1;

      mux4bit21 mux0(.clk(clk),
                  .reset_L(reset_L),
                  .selector(selector[1]),
                  .data_in0(data_in0[3:0]),
                  .valid_0(valid_0),
                  .data_in1(data_in1[3:0]),
                  .valid_1(valid_1),
                  .valid_out(validOut0),
                  .data_out_cond(outMux0));

      mux4bit21 mux1(.clk(clk),
                  .reset_L(reset_L),
                  .selector(selector[1]),
                  .data_in0(data_in2[3:0]),
                  .valid_0(valid_2),
                  .data_in1(data_in3[3:0]),
                  .valid_1(valid_3),
                  .valid_out(validOut1),
                  .data_out_cond(outMux1));
      
      mux4bit21 muxOut(.clk(clk),
                  .reset_L(reset_L),
                  .selector(selector[0]),
                  .data_in0(outMux0[3:0]),
                  .valid_0(validOut0),
                  .data_in1(outMux1[3:0]),
                  .valid_1(validOut1),
                  .valid_out(valid_out),
                  .data_out_cond(data_out_cond));
   
endmodule