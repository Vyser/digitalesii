// `timescale 	1ns	/ 100ps
// escala	unidad temporal (valor de "#1") / precisi�n

// includes de archivos de verilog
// Pueden omitirse y llamarse desde el makefile
`include "lib/cmos_cells.v"
`include "src/mux4bit41/mux4bit41.v"
`include "src/mux4bit41/mux4bit41_synth.v"
`include "src/mux4bit41/probador.v"

module BancoPruebas; // Testbench
	// Por lo general, las se�ales en el banco de pruebas son wires.
	// No almacenan un valor, son manejadas por otras instancias de m�dulos.
	wire reset_L, valid_0, valid_1, valid_2, valid_3, valid_out, valid_out_estr;
	wire [1:0] selector;
	wire[3:0] data_in0, data_in1, data_in2, data_in3, data_out_cond, data_out_estr;

	// Descripcion conductual del circuito
	mux4bit41	muxConductual(/*AUTOINST*/
				      // Outputs
				      .valid_out	(valid_out),
				      .data_out_cond	(data_out_cond[3:0]),
				      // Inputs
				      .clk		(clk),
				      .reset_L		(reset_L),
				      .selector		(selector[1:0]),
				      .data_in0		(data_in0[3:0]),
				      .valid_0		(valid_0),
				      .data_in1		(data_in1[3:0]),
				      .valid_1		(valid_1),
				      .data_in2		(data_in2[3:0]),
				      .valid_2		(valid_2),
				      .data_in3		(data_in3[3:0]),
				      .valid_3		(valid_3));

	// Descripcion estructural del circuito
	mux4bit41_synth muxSintetizado(/*AUTOINST*/
				       // Outputs
				       .data_out_estr	(data_out_estr[3:0]),
				       .valid_out_estr	(valid_out_estr),
				       // Inputs
				       .clk		(clk),
				       .data_in0	(data_in0[3:0]),
				       .data_in1	(data_in1[3:0]),
				       .data_in2	(data_in2[3:0]),
				       .data_in3	(data_in3[3:0]),
				       .reset_L		(reset_L),
				       .selector	(selector[1:0]),
				       .valid_0		(valid_0),
				       .valid_1		(valid_1),
				       .valid_2		(valid_2),
				       .valid_3		(valid_3));
					  
	// Probador: generador de señales y monitor
	probador prob (/*AUTOINST*/
		       // Outputs
		       .clk		(clk),
		       .reset_L		(reset_L),
		       .selector	(selector[1:0]),
		       .data_in0	(data_in0[3:0]),
		       .data_in1	(data_in1[3:0]),
		       .data_in2	(data_in2[3:0]),
		       .data_in3	(data_in3[3:0]),
		       .valid_0		(valid_0),
		       .valid_1		(valid_1),
		       .valid_2		(valid_2),
		       .valid_3		(valid_3),
		       // Inputs
		       .valid_out	(valid_out),
		       .valid_out_estr	(valid_out_estr),
		       .data_out_cond	(data_out_cond[3:0]),
		       .data_out_estr	(data_out_estr[3:0]));
endmodule
