module mux2bit21( input clk,
            input reset_L,
            input selector,
            input [1:0] data_in0,
            input valid_0,
            input [1:0] data_in1,
            input valid_1,
            output reg valid_out,
            output reg[1:0] data_out_cond
      );

    reg[1:0] salMux1 = 2'b00;
    reg validInter;

    always @(*) begin
    	if (selector == 1) begin
            validInter = valid_1;
    	    salMux1 = data_in1;
    	end else begin
            validInter = valid_0;
    	    salMux1 = data_in0;
    	end
    end

    always @(posedge clk) begin
        if (reset_L == 1) begin
            if(valid_out == 1) begin
                data_out_cond <= salMux1;
            end
            valid_out <= validInter;
        end else begin
            data_out_cond <= 2'b00;
            valid_out <= 0;
        end
    end
endmodule