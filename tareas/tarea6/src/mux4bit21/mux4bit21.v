`include "src/mux2bit21/mux2bit21.v"

module mux4bit21(input clk,
				 input reset_L,
				 input selector,
				 input [3:0] data_in0,
				 input valid_0,
				 input [3:0] data_in1,
				 input valid_1,
				 output valid_out,
				 output [3:0] data_out_cond
		);
		mux2bit21 mux1( .clk(clk),
						.reset_L(reset_L),
						.selector(selector),
						.data_in0(data_in0[3:2]),
						.valid_0(valid_0),
						.data_in1(data_in1[3:2]),
						.valid_1(valid_1),
						.valid_out(valid_out),
						.data_out_cond(data_out_cond[3:2]));
	
		mux2bit21 mux0( .clk(clk),
						.reset_L(reset_L),
						.selector(selector),
						.data_in0(data_in0[1:0]),
						.valid_0(valid_0),
						.data_in1(data_in1[1:0]),
						.valid_1(valid_1),
						.valid_out(valid_out),
						.data_out_cond(data_out_cond[1:0]));

endmodule