`timescale 1ns/100ps
module probador( // M�dulo probador: generador de se�ales y monitor de datos
	output reg clk,
	output reg		reset_L,
	output reg		selector,
	output reg[3:0]	data_in0,
	output reg[3:0]	data_in1,
	output reg valid_0,
	output reg valid_1,
	input valid_out,
	input valid_out_estr,
	input [3:0]	data_out_cond,
	input [3:0] data_out_estr);

	reg [7:0] cnt = 'b0;

	// reg 		clk;
	// Bloque de procedimiento, no sintetizable, se recorre una �nica vez
	// Generalmente, los initial s�lo se usan en los testbench o probadores
	// Se recomienda siempre sincronizar con el reloj y utilizar
	// asignaciones no bloqueantes en la generaci�n de se�ales
	initial begin
		$dumpfile("mux.vcd");	// Nombre de archivo del "dump"
		$dumpvars;			// Directiva para "dumpear" variables
		// Mensaje que se imprime en consola una vez
		$display ("\t\ttime\tclk\treset_L\tselector\tdata_in0\tvalid_0\tdata_in1\tvalid_1\tdata_out_estr\tvalid_out_estr\tdata_out_cond\tvalid_out_cond\tcuenta");
		// Mensaje que se imprime en consola cada vez que un elemento de la lista cambia
		$monitor($time,"\t%b\t%b\t%b\t%b\t%b\t%b\t%b\t%b\t%b\t%b\t%b\t%d", clk, reset_L, selector, data_in0, valid_0, data_in1, valid_1, data_out_estr, valid_out_estr, data_out_cond, valid_out, cnt);
		reset_L <= 1'b0;
		selector <= 1'b0;
		valid_0 <= 1'b1;
		valid_1 <= 1'b1;
		{data_in0, data_in1} = {4'b0, 4'b0};
		@(posedge clk);
		reset_L <= 1'b1;
		@(posedge clk);
		data_in0 <= 4'b1111;
		data_in1 <= 4'b1100;
		@(posedge clk);
		selector <= 1'b1;
		data_in0 <= 4'b0011;
		data_in1 <= 4'b0000;
		@(posedge clk);
		valid_0 <= 1'b0;
		valid_1 <= 1'b0;
		data_in0 <= 4'b0000;
		data_in1 <= 4'b1100;
		@(posedge clk);
		selector <= 1'b0;
		data_in0 <= 4'b1111;
		data_in1 <= 4'b1111;
		@(posedge clk);
		valid_0 <= 1'b1;
		selector <= 1'b1;
		data_in0 <= 4'b0000;
		data_in1 <= 4'b0011;
		@(posedge clk);
		selector <= 1'b0;
		data_in0 <= 4'b1100;
		data_in1 <= 4'b0000;
		repeat (4) begin
    	@(posedge clk);
    	end
		$finish;			// Termina de almacenar se�ales
	end // initial begin

	always @ (posedge clk) begin
      	if (reset_L) begin
         	if (data_out_cond != data_out_estr)
            	$display("Inconsistencia a los %t", $realtime);
      	end
		  	if (data_out_estr == data_out_cond) begin
			  	$display("Todo bien");
		end
   	end

	always @ (posedge data_out_estr[0]) begin
		cnt = cnt + 1;
	end
	always @ (posedge data_out_estr[1]) begin
		cnt = cnt + 1;
	end
	always @ (posedge data_out_estr[2]) begin
		cnt = cnt + 1;
	end
	always @ (posedge data_out_estr[3]) begin
		cnt = cnt + 1;
	end

	// Reloj
	initial	clk 	<= 0;			// Valor inicial al reloj, sino siempre ser� indeterminado
	always	#4.5 clk 	<= ~clk;		// Hace "toggle" cada 2*1100ns
endmodule
