`timescale	1ns	/100ps

`include "paramMux_cond.v"
`include "paramMux_estruc.v"
`include "probador.v"
`include "cmos_cells.v"

module bancoPruebas#(parameter BUS_SIZE = 16, 
	parameter WORD_SIZE = 4, 
	parameter WORD_NUM = BUS_SIZE/WORD_SIZE);

    wire	clk,reset;  
    wire	[BUS_SIZE-1:0] data_in;
    wire	[BUS_SIZE-1:0] data_out_cond;
    wire	[WORD_NUM-1:0] control_cond;
    wire	error_cond;
	wire	[BUS_SIZE-1:0] data_out_estruc;
    wire	[WORD_NUM-1:0] control_estruc;
    wire	error_estruc;

    paramMux_cond    paramMux_cond(/*AUTOINST*/
				   // Outputs
				   .data_out_cond	(data_out_cond[BUS_SIZE-1:0]),
				   .control_cond	(control_cond[WORD_NUM-1:0]),
				   .error_cond		(error_cond),
				   // Inputs
				   .clk			(clk),
				   .reset		(reset),
				   .data_in		(data_in[BUS_SIZE-1:0]));

    probador  test(/*AUTOINST*/
		   // Outputs
		   .clk			(clk),
		   .reset		(reset),
		   .data_in		(data_in[BUS_SIZE-1:0]),
		   // Inputs
		   .data_out_cond	(data_out_cond[BUS_SIZE-1:0]),
		   .control_cond	(control_cond[WORD_NUM-1:0]),
		   .error_cond		(error_cond),
		   .data_out_estruc	(data_out_estruc[BUS_SIZE-1:0]),
		   .control_estruc	(control_estruc[WORD_NUM-1:0]),
		   .error_estruc	(error_estruc));

   paramMux_estruc    paramMux_estruc(/*AUTOINST*/
				      // Outputs
				      .control_estruc	(control_estruc[3:0]),
				      .data_out_estruc	(data_out_estruc[15:0]),
				      .error_estruc	(error_estruc),
				      // Inputs
				      .clk		(clk),
				      .data_in		(data_in[15:0]),
				      .reset		(reset));
	
endmodule
