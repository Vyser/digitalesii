module paramMux_cond#(parameter BUS_SIZE = 16,
    parameter WORD_SIZE = 4, 
    parameter WORD_NUM = (BUS_SIZE/WORD_SIZE))
    (input clk,
    input reset,    
    input [BUS_SIZE-1:0] data_in,
    output reg [BUS_SIZE-1:0] data_out_cond,
    output reg [WORD_NUM-1:0] control_cond,
    output reg  error_cond);

    reg [3:0] state, next_state;
    reg [WORD_SIZE-1:0] counter, next_count, ones, aux;
    reg next_err;

    parameter RESET = 0;
    parameter FIRST_PKT = 1;
    parameter REG_PKT = 2;
    parameter F_ERROR = 3;
    parameter SEQ_ERROR = 4;

    always @ (posedge clk) begin
        if(~reset) begin
            state <= RESET;
            counter <= 0;
            error_cond <=0;
        end
        else begin
            state <= next_state;
            counter <= next_count;
            error_cond <= next_err;
        end
    end

    generate
        genvar i;
        genvar j;
        for(i = 0; i < (WORD_NUM); i = i + 1) begin: gen1 
            for(j = 0; j < (WORD_SIZE); j = j +1) begin: gen2 
                always @ (*) begin
                    if (data_in[i*WORD_SIZE + WORD_SIZE-1:i * WORD_SIZE] == 0)
                        control_cond[i] = 0;
                    else
                        control_cond[i] = 1;
                    data_out_cond[j + i*WORD_SIZE] = data_in[BUS_SIZE-WORD_SIZE+j-i*WORD_SIZE];
                    ones[i] = 1;			
                end
            end
        end
    endgenerate

    always @ (*) begin
		next_state = state;
        next_count = counter;
        next_err = error_cond;
        aux = data_in[WORD_SIZE-1:0];

        case (state)

			RESET: begin
                next_state = FIRST_PKT;
                next_err = 0;
                next_count = 0;
            end

            FIRST_PKT: begin
                if(data_in[WORD_SIZE-1:0] != counter) begin
                    next_state = SEQ_ERROR;
                    next_err = 1;
                    next_count = 0;
                end
                else if(data_in[BUS_SIZE-1:BUS_SIZE-WORD_SIZE] != ones) begin
                    next_state = F_ERROR;
                    next_err = 1;
                    next_count = 0;
                end
                else begin
                    next_state = REG_PKT;
                    next_err = 0;
                    next_count = counter +1;
                end
            end

            REG_PKT: begin
                if(data_in[WORD_SIZE-1:0] != counter) begin
                    next_state = SEQ_ERROR;
                    next_err = 1;
                    next_count = 0;
                end
                else if(data_in[BUS_SIZE-1:BUS_SIZE-WORD_SIZE] != ones) begin
                    next_state = F_ERROR;
                    next_err = 1;
                    next_count = 0;
                end
                else begin
                    next_state = REG_PKT;
                    next_err = 0;
                    next_count = counter +1;
                end
            end

            F_ERROR: begin
                next_count = 1;
                if(data_in[BUS_SIZE-1:BUS_SIZE-WORD_SIZE] != ones) begin
                    next_state = F_ERROR;
                    next_err = 1;
                end
                else if(data_in[WORD_SIZE-1:0] != counter) begin
                    next_state = SEQ_ERROR;
                    next_err = 0;
                end
                else begin
                    next_state = FIRST_PKT;
                    next_err = 0;
                end
            end

            SEQ_ERROR: begin
                next_count = 1;
                if(data_in[BUS_SIZE-1:BUS_SIZE-WORD_SIZE] != ones) begin
                    next_state = F_ERROR;
                    next_err = 1;
                end
                else if(data_in[WORD_SIZE-1:0] != counter) begin
                    next_state = SEQ_ERROR;
                    next_err = 0;
                end
                else begin
                    next_state = FIRST_PKT;
                    next_err = 0;
                end
            end

            default: begin
                next_count = 0;
                next_state = FIRST_PKT;
                next_err = 0;
            end
        endcase
    end

endmodule



