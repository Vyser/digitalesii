module probador#(parameter BUS_SIZE = 16, 
	parameter WORD_SIZE = 4, 
	parameter WORD_NUM = BUS_SIZE/WORD_SIZE)(
    output reg clk,
	output reg reset,    
    output reg [BUS_SIZE-1:0] data_in,
    input [BUS_SIZE-1:0] data_out_cond,
    input [WORD_NUM-1:0] control_cond,
    input  error_cond,
	input [BUS_SIZE-1:0] data_out_estruc,
    input [WORD_NUM-1:0] control_estruc,
    input  error_estruc);
	
	reg [3:0] aux;

	initial begin
		$dumpfile("paramMux.vcd");
		$dumpvars;
		
		data_in <= 16'hFBA0;
		reset <= 0;
		#20 reset <= 1;
		@(posedge clk);
		repeat (2) begin
			@(posedge clk);	
			data_in <= data_in + 1;
		end
		@(posedge clk);
		data_in <= 16'hABA3;
		@(posedge clk);
		data_in <= 16'hFBA0;
		@(posedge clk);
		data_in <= 16'hFBA9;
		repeat (4) begin
			@(posedge clk);
		end
		
		$finish;
	end

	initial	clk 	<= 0;
	always	#15 clk 	<= ~clk;

endmodule
