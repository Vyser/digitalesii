// `timescale 1ns/100ps
`include "my_1bit_mux.v"

module my_2bit_mux(input [1:0] A,
                   input [1:0] B,
                   input SEL,
                   output [1:0] OUT);
    
    my_1bit_mux mux0( .A(A[0]),
                      .B(B[0]),
                      .SEL(SEL),
                      .OUT(OUT[0]));

    my_1bit_mux mux1( .A(A[1]),
                      .B(B[1]),
                      .SEL(SEL),
                      .OUT(OUT[1]));

endmodule