`timescale 1ns/100ps

module my_and (input A, // Representación de la compuerta SN74AUP2G08, $0.35 unidad,
              input B,
              output OUT);
    assign #(0.9:3.3:5.5, 0.9:3.3:5.5) OUT = A & B; // Vcc = 3.3 V, CL = 15 pF
endmodule