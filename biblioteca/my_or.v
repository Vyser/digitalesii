`timescale 1ns/100ps

module my_or (input A, // Representación de la compuerta SN74LVC2G32-Q1, $0.46 unidad,
              input B,
              output OUT);
    assign #(0:1:5.8, 0:1:5.8) OUT = A | B; // Vcc = 3.3 V
endmodule