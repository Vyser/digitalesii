`timescale 1ns/100ps

module my_flop (input [1:0] D, // Representación del FFD SN74LVC1G80-Q1, $0.2 unidad, tpd(1.3:4.2)
                input clk,
                output reg [1:0] Q);
    always @(posedge clk)
        #4.2
        Q <= D;
endmodule