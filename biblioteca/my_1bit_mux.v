// `timescale 1ns/100ps
`include "include.v"

module my_1bit_mux(input A, // Representación del diagrama del IC SN74LVC2G157, $1.11 unidad
              input B,
              input SEL,
              output OUT);
    wire salAndA_notSel, salAndB_Sel, salNotSel;
    my_not notSel( .IN(SEL),
                   .OUT(salNotSel));
    
    my_and andA_notSel ( .A(A),
                         .B(salNotSel),
                         .OUT(salAndA_notSel));

    my_and andB_Sel ( .A(B),
                      .B(SEL),
                      .OUT(salAndB_Sel));

    my_or orOUT ( .A(salAndA_notSel),
                  .B(salAndB_Sel),
                  .OUT(OUT));
endmodule