`timescale 1ns/100ps

module my_not (input IN, //Representación de la compuerta SN74LV1T04, $0.22 unidad, CL = 15 pF
              output OUT);
    assign #(0:4:5, 0:4:5) OUT = ~IN;//La hoja del fabricante no indica tiempo mínimo 
endmodule